<?php

namespace elanpl\L3\HTMLSerializer;

class HTMLSerializer implements \elanpl\L3\ISerializer{
  
    public function serialize($viewModel)
    {
        if(is_object($viewModel) && ($viewModel instanceof \elanpl\L3\ViewModel)){
            
            $view = new \elanpl\L3\View($viewModel);

            return $view->render();
        }
        throw new \Exception("Invalid argument type for HTML Serializer!");
    }
}
